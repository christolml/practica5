<html>
<head>
    <title>Que onda</title>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" ></script>

    <script src="https://www.gstatic.com/firebasejs/5.5.9/firebase.js"></script>
    <script>
    // Initialize Firebase
    var config = {
        apiKey: "AIzaSyAvIJth8rsfNe8hUZCE9FAsycbqAhtKqZE",
        authDomain: "practica5-fe0b3.firebaseapp.com",
        databaseURL: "https://practica5-fe0b3.firebaseio.com",
        projectId: "practica5-fe0b3",
        storageBucket: "practica5-fe0b3.appspot.com",
        messagingSenderId: "82389512259"
    };
    firebase.initializeApp(config);

    // Retrieve Firebase Messaging object.
    const messaging = firebase.messaging();

    messaging.requestPermission().then(function() {
    console.log('Notification permission granted.');
    getRegToken();

    }).catch(function(err) {
    console.log('Unable to get permission to notify.', err);
    });



    function getRegToken() {

        messaging.getToken().then(function(currentToken) {
        if (currentToken) {
            saveToken(currentToken);
            console.log(currentToken);
            setTokenSentToServer(true);
        } else {
            console.log('No Instance ID token available. Request permission to generate one.');
            setTokenSentToServer(false);
        }
        }).catch(function(err) {
            console.log('An error occurred while retrieving token. ', err);
            setTokenSentToServer(false);
        });

    }

    function saveToken(currentToken) {
        if (isTokenSentToServer()) {
            console.log('El token ya fue enviado');
            setTokenSentToServer(true);
        } else {
            $.ajax({
            url:'action.php',
            method:'post',
            data: 'token=' + currentToken
            }).done(function(result) {
                console.log(result);
            })
        }
    }


    function isTokenSentToServer() {
      return window.localStorage.getItem('sentToServer') == 1;
    }
    
    function setTokenSentToServer(sent) {
      window.localStorage.setItem('sentToServer', sent ? 1 : 0);
    }

    messaging.onMessage(function(payload){
    var title = payload.data.title;
    var options = {
    body: payload.data.body,
    icon: payload.data.icon
    }
    var myNotification = new Notification(title, options);
    console.log('Mensaje recibidio', payload)
    })
    </script>


    <link rel="manifest" href="manifest.json">

</head>
<body>
    <h1>App con notificaciones</h1>
    
</body>
</html>